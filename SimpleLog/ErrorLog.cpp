#include "ErrorLog.h"

#include <fstream>

#ifdef _WIN32
#define _WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

SimpleLog::ErrorLog::ErrorLog()
{}

void SimpleLog::ErrorLog::Log(std::string errorText, ErrorSeverity severity)
{
	std::string message = "";

#ifdef _WIN32
	HANDLE Stdout = GetStdHandle(STD_OUTPUT_HANDLE);
#endif

	const char* CSI = "\33[";
	switch (severity)
	{
	case ErrorSeverity::OpComplete:
#ifdef _WIN32
		SetConsoleTextAttribute(Stdout, FOREGROUND_GREEN);
		printf("Operation Complete ");
#else
		printf("%s%sOperation Complete ", CSI, "32m");
#endif
		message += "Operation Complete ";
		break;
	case ErrorSeverity::Warning:
#ifdef _WIN32
		SetConsoleTextAttribute(Stdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		printf("Warning ");
#else
		printf("%s%sWarning ", CSI, "33m");
#endif
		message += "Warning ";
		break;
	case ErrorSeverity::Error:
#ifdef _WIN32
		SetConsoleTextAttribute(Stdout, FOREGROUND_RED);
		printf("Error ");
#else
		printf("%s%sError ", CSI, "31m");
#endif
		message += "Error ";
		break;
	case ErrorSeverity::FatalError:
#ifdef _WIN32
		SetConsoleTextAttribute(Stdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
		printf("Fatal Error ");
#else
		printf("%s%sFatal Error ", CSI, "91m");
#endif
		message += "Fatal Error ";
		break;
	};

	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d.%I:%M:%S %p", &tstruct);
	printf("[%s]: ", buf);
	message += "[";
	message += std::string(buf);
	message += "]: ";

#ifdef _WIN32
	SetConsoleTextAttribute(Stdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
#else
	printf("%s%s", CSI, "97m");
#endif

	printf("%s\n", errorText.c_str());
	message += errorText;

	std::ofstream file;
	file.open("log.txt", std::ios::out | std::ios::app);
	file << message;
	file << "\n";
	file.close();
}
