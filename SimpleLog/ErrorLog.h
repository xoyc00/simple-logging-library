#ifndef _ERROR_LOG
#define _ERROR_LOG

#include <stdio.h>
#include <time.h>
#include <string>

namespace SimpleLog
{
	enum class ErrorSeverity
	{
		OpComplete,
		Warning,
		Error,
		FatalError
	};

	class ErrorLog
	{
	public:
		ErrorLog();

		void Log(std::string errorText, ErrorSeverity severity);
	};
}

#endif
